/* Marmote and MarmoteMDP are free softwares: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Marmote is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Marmote. If not, see <http://www.gnu.org/licenses/>.

Copyright 2022 Emmanuel Hyon, Alain Jean-Marie*/



#include <marmoteCore/marmoteSparseMatrix.h>
#include <marmoteCore/marmoteInterval.h>
#include <marmoteMDP/marmoteDiscountedMDP.h>
#include <marmoteMDP/marmoteFeedbackSolutionMDP.h>
#include <marmoteMDP/marmoteSolutionMDP.h>


#include <list>
#include <vector>
#include <string>


using namespace std;

/**
 * @brief class to implement an example of a simple discounted MDP
 * @author Hyon, lip6
 * @date Apr 2022
 * @version 1.1
 *
 * This example create two example of discounted MDP 
 * 
 * First MDP :
 * 
 * one gives only only one reward matrix and we construct on object MDP
 * 
 * We should obtain
 * 
 * v= (12,28763; 11,31989)
 * 
 * Second MDP we enter two matrices for the reward. 
 * One for each action
 * one entry (i,j) of matrix associated with action a
 * is the reward jumping from i to j while triggering action a 
 * R(j|(i,a))
 * 
 * The final matrix of reward should be
 * [ 0.725  4.025 ]
 * [ 3.125  2.025]
 * 
 * 
 * The value function we should get
 * v= (12,28763; 11,31989)
 * 
*/




int main( int argc, char** argv )
{

/* create and initialize the MDP parameters discount factor and criteria. */
double beta = 0.7;
string critere("max");

/*create and initialize the maximum number of iterations allowed end the precision.*/
int maxIter = 700;
double epsilon = 0.00001;




/* Create the MDP object to test 1-dimension example and fill all its fields.*/
int min = 0;
int max = 1;

/* Create the spaces */
MarmoteSet *actionSpace = new MarmoteInterval(0,1);
MarmoteSet *stateSpace = new MarmoteInterval(min,max);

/*Create the vector for the transition structures (here matrices) */
vector<TransitionStructure*> trans(actionSpace->Cardinal());

/*create transition matrices */
SparseMatrix *P1 = new SparseMatrix(2);
P1->addToEntry(0,0,0.2);
P1->addToEntry(0,1,0.8);
P1->addToEntry(1,0,0.4);
P1->addToEntry(1,1,0.6);
/* ajout a la collection */
trans.at(0) = P1;

SparseMatrix *P2 = new SparseMatrix(2);
P2->addToEntry(0,0,0.5);
P2->addToEntry(0,1,0.5);
P2->addToEntry(1,0,0.7);
P2->addToEntry(1,1,0.3);
trans.at(1) = P2;

/* input of the reward matrix
 * one entry is r(x,a)
 * with x in line the index of the state
 * with a in column the index of the action
*/
SparseMatrix *R1  = new SparseMatrix(2);
    
R1->addToEntry(0,0,0.725);
R1->addToEntry(0,1,4.025);
R1->addToEntry(1,0,3.125);
R1->addToEntry(1,1,2.025);
    
std::cout << critere.size() << std::endl;
cout << "Begining MDP building" << std::endl ;
/* Create MDP object */
DiscountedMDP *mdp1 = new DiscountedMDP(critere, stateSpace, actionSpace, trans, R1,beta);
cout << "End of building MDP" << endl ;

cout << "printing MDP" << endl;
mdp1->WriteMDP();
        
cout << "Printing solution from value iteration" << endl;
/*call the function to solve the MDP.*/
SolutionMDP *optimum = mdp1->ValueIteration(epsilon, maxIter);
optimum->WriteSolution();
        
cout << endl << "Checking solutions" << endl;
double *sol1 = mdp1->PolicyCost(optimum,epsilon, maxIter);
for(int i=0;i<stateSpace->Cardinal();i++){
       cout << "i= " << i << " sol= " << sol1[i] << endl;  
}
        
/*call the function to solve the MDP.*/
cout << "Printing solution from modified value iteration" << endl;
SolutionMDP *optimum2 = mdp1->PolicyIterationModified(epsilon, maxIter, 0.01, 100);
optimum2->WriteSolution();

cout << endl << "********************************" << endl;
cout << endl << "********************************" << endl;


/* creating of a vector of cost matrices when cost are determined by transitions */
vector<TransitionStructure*> rews(actionSpace->Cardinal());
/* matrices des gains par transitions */
SparseMatrix *R2 = new SparseMatrix(2);
SparseMatrix *R3  = new SparseMatrix(2);
    
R2->addToEntry(0,0,3.625);
R2->addToEntry(0,1,0);
R2->addToEntry(1,0,0.3125);
R2->addToEntry(1,1,5);

R3->addToEntry(0,0,4.025);
R3->addToEntry(0,1,4.025);
R3->addToEntry(1,0,0);
R3->addToEntry(1,1,6.75);

/* Adding reward to collection */
rews.at(0) = R2;
rews.at(1) = R3;

/* Create the second spaces 
 * Otherwise the sharing of the same elements is problematic 
*/
MarmoteSet *actionSpace2 = new MarmoteInterval(0,1);
MarmoteSet *stateSpace2 = new MarmoteInterval(min,max);


cout << endl << "Building second MDP" << endl;
DiscountedMDP *mdp2 = new DiscountedMDP(critere, stateSpace2, actionSpace2, trans, rews,beta);
mdp2->WriteMDP();
cout << "Second MDP built and printed" << endl;


cout << "Printing solution from value iteration (second MDP)" << endl;
/*call the function to solve the MDP.*/
SolutionMDP *optimum3 = mdp2->ValueIteration(epsilon, maxIter);
optimum3->WriteSolution();
   
cout << endl << "Checking solutions" << endl;
double *sol2 = mdp2->PolicyCost(optimum3,epsilon, maxIter);
for(int i=0;i<stateSpace->Cardinal();i++){
     cout << "i= " << i << " sol= " << sol2[i] << endl; 
}
        
/*Call the function to solve the MDP and print it.*/
cout << "Printing solution from modified value iteration" << endl;
SolutionMDP *optimum4 = mdp2->PolicyIterationModified(epsilon, maxIter, 0.01, 100);
optimum4->WriteSolution();
     
     
cout << "Destructing" << endl;
mdp1->ClearRew();
mdp2->ClearRew();

    
delete mdp1;
delete mdp2;

delete optimum;
delete optimum2;
delete optimum3;
delete optimum4;
    
cout << "Destructing 2" << endl;
//delete actionSpace;
//delete stateSpace;

cout << "Destructing 3" << endl;
        
delete P1;
delete P2;
/* delete R1; is not usefull since it has been deleted with the deleting of mdp1 */
delete R2;
delete R3;

        
delete[] sol1;
delete[] sol2;

return 0;
}


