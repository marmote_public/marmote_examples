[[_TOC_]]


# marmote_examples

## Purpose

The purpose of this repository is to be a ready to use set of examples on how to use the [marmote library](https://marmote.gitlabpages.inria.fr/marmote/)

## Getting Started

### Requirement

The recommended way is to [install miniconda3](https://docs.anaconda.com/free/miniconda/miniconda-install/).

### Installation and Build of the Examples
```bash
git clone https://gitlab.inria.fr/marmote_public/marmote_examples.git
cd marmote_examples
export MARMOTE_EXAMPLES_ROOT=${PWD} 
conda env update -f pkg/env/marmote-use.yaml
conda activate marmote-use
mkdir build
cd build
cmake ..
make -j4
```

### Running the Examples
You can find at https://marmote.gitlabpages.inria.fr/marmote/examples.html the description of the examples.
To run them, simply 
```bash
conda activate marmote-use # if not already in the marmote-use environment
${MARMOTE_EXAMPLES_ROOT}/build/example5
```
